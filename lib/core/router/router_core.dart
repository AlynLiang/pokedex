import 'package:go_router/go_router.dart';

import 'package:pokedex/modules/error/error_module.dart';
import 'package:pokedex/modules/home/home_module.dart';
import 'package:pokedex/modules/poke_info/page/poke_info_page.dart';

mixin RouterCore {
  static final router = GoRouter(
    errorBuilder: (context, state) => const ErrorPage(),
    routes: [
      GoRoute(
        path: '/',
        builder: (context, state) => const HomePage(),
      ),
      GoRoute(
        path: '/pokemon/:id',
        builder: (context, state) =>
            PokeInfoPage(id: state.params['id']!),
      ),
    ],
  );
}
