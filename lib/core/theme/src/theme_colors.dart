import 'package:flutter/material.dart';

mixin ThemeColors {
  static Color black = Colors.black;
  static Color red = const Color(0xffcc0000);
  static Color white = Colors.white;
}
