import 'package:flutter/material.dart';

import 'src/theme_colors.dart';

mixin ThemeController {
  static ThemeData themeData = ThemeData(
    useMaterial3: true,
    brightness: Brightness.light,
    colorSchemeSeed: ThemeColors.red,
    appBarTheme: AppBarTheme(
      backgroundColor: ThemeColors.red,
      foregroundColor: ThemeColors.white,
    ),
  );
}
