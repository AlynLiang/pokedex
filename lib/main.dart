import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokedex/modules/poke_info/bloc/poke_info_bloc.dart';
import 'package:url_strategy/url_strategy.dart';

import 'package:pokedex/core/router/router_core.dart';
import 'package:pokedex/core/theme/theme_controller.dart';
import 'package:pokedex/modules/home/home_module.dart';

void main() {
  setPathUrlStrategy();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => HomeBloc(),
        ),
        BlocProvider(
          create: (context) => PokeInfoBloc(),
        ),
      ],
      child: MaterialApp.router(
        title: 'Pokedex',
        theme: ThemeController.themeData,
        routerConfig: RouterCore.router,
      ),
    ),
  );
}
