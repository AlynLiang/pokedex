import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokedex/modules/home/model/home_model.dart';
import 'package:pokedex/modules/home/repository/home_repository.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial()) {
    on<HomeFetchEvent>(_fetch);
    on<HomeChangePageEvent>(_changePage);
  }

  void _fetch(HomeFetchEvent event, Emitter<HomeState> emit) async {
    emit(HomeFetch());

    HomeModel? data = await HomeRepository.fetchPokemonList(
      offset: event.offset,
      limit: event.limit,
    );

    if (data != null) {
      emit(HomeLoadedList(data));
    } else {
      emit(HomeEmptyList());
    }
  }

  void _changePage(HomeChangePageEvent event, Emitter<HomeState> emit) async {
    emit(HomeFetch());

    HomeModel? data =
        await HomeRepository.changePokemonListPage(url: event.url);

    if (data != null) {
      emit(HomeLoadedList(data));
    } else {
      emit(HomeEmptyList());
    }
  }
}
