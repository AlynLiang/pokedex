part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class HomeFetchEvent extends HomeEvent {
  final int offset;
  final int limit;

  HomeFetchEvent(
    this.offset,
    this.limit,
  );
}

class HomeChangePageEvent extends HomeEvent {
  final String url;

  HomeChangePageEvent(this.url);
}
