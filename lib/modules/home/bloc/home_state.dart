part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class HomeFetch extends HomeState {}

class HomeEmptyList extends HomeState {}

class HomeLoadedList extends HomeState {
  final HomeModel data;

  HomeLoadedList(this.data);
}
