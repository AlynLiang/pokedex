class HomeModel {
  HomeModel({
    required this.count,
    this.next,
    this.previous,
    required this.results,
  });

  int count;
  String? next;
  String? previous;
  List<Result> results;

  HomeModel.fromJson(Map<String, dynamic> json)
      : count = json['count'],
        next = json['next'],
        previous = json['previous'],
        results = [] {
    for (var element in json['results']) {
      results.add(
        Result(
          name: element['name'],
          url: element['url'],
          id: int.tryParse(
                  Uri.parse(element['url']).pathSegments[
                      Uri.parse(element['url']).pathSegments.length - 2],
                  radix: 10) ??
              0,
        ),
      );
    }
  }

  Map<String, dynamic> toJson() => {
        'count': count,
        'next': next,
        'previous': previous,
        'results': results,
      };
}

class Result {
  Result({
    required this.name,
    required this.url,
    required this.id,
  });

  String name;
  String url;
  int id;
}
