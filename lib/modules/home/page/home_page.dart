import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import 'package:pokedex/modules/home/home_module.dart';
import 'package:pokedex/modules/home/page/widgets/list_filter_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    BlocProvider.of<HomeBloc>(context).add(HomeFetchEvent(1, 10));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
            child: Column(
          children: [
            const ListFilterWidget(),
            BlocBuilder<HomeBloc, HomeState>(
              builder: (context, state) {
                if (state is HomeFetch) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (state is HomeEmptyList) {
                  return const Center(
                    child: Text('List is empty'),
                  );
                } else if (state is HomeLoadedList) {
                  List<Result> data = state.data.results;
                  return ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) => ListTile(
                      leading:
                          Text((data[index].id).toString().padLeft(3, '0')),
                      title: Text(data[index].name),
                      onTap: () =>
                          GoRouter.of(context).go('/pokemon/${data[index].id}'),
                    ),
                    separatorBuilder: (context, index) => const Divider(),
                    itemCount: data.length,
                  );
                } else {
                  return const Center(
                    child: Text('Unexpected error'),
                  );
                }
              },
            ),
          ],
        )),
      ),
    );
  }
}
