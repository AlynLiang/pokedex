// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:pokedex/modules/home/home_module.dart';

class ListFilterWidget extends StatelessWidget {
  const ListFilterWidget({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController offsetController = TextEditingController(text: '1');
    TextEditingController limitController = TextEditingController(text: '10');

    void refreshList() {
      BlocProvider.of<HomeBloc>(context).add(
        HomeFetchEvent(
          int.tryParse(offsetController.text) ?? 1,
          int.tryParse(limitController.text) ?? 10,
        ),
      );
    }

    return Column(
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: TextField(
                keyboardType: TextInputType.number,
                controller: offsetController,
                onEditingComplete: () => refreshList(),
              ),
            ),
            Expanded(
              child: TextField(
                controller: limitController,
                onEditingComplete: () => refreshList(),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ElevatedButton(
            onPressed: () => refreshList(),
            child: const Text('Refresh'),
          ),
        ),
        BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is HomeLoadedList) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                        onPressed: () {
                          if (state.data.previous != null) {
                            BlocProvider.of<HomeBloc>(context)
                                .add(HomeChangePageEvent(state.data.previous!));
                          }
                        },
                        child: Text('previous')),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:
                        ElevatedButton(onPressed: () {
                          if (state.data.next != null) {
                            BlocProvider.of<HomeBloc>(context)
                                .add(HomeChangePageEvent(state.data.next!));
                          }
                        }, child: Text('next')),
                  ),
                ],
              );
            } else {
              return const SizedBox();
            }
          },
        )
      ],
    );
  }
}
