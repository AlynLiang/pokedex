import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:pokedex/modules/home/model/home_model.dart';

mixin HomeRepository {
  static Future<HomeModel?> fetchPokemonList({
    int offset = 0,
    int limit = 0,
  }) async {
    HomeModel? data;

    var url = Uri.parse(
        'https://pokeapi.co/api/v2/pokemon/?offset=${offset - 1}&limit=$limit');
    final result = await http.get(url);

    if (result.statusCode == 200) {
      data = HomeModel.fromJson(jsonDecode(result.body));
    }

    return data;
  }

  static Future<HomeModel?> changePokemonListPage({
    required String url,
  }) async {
    HomeModel? data;

    var uri = Uri.parse(url);
    final result = await http.get(uri);

    if (result.statusCode == 200) {
      data = HomeModel.fromJson(jsonDecode(result.body));
    }

    return data;
  }
}
