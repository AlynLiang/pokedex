import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokedex/modules/poke_info/model/poke_info_model.dart';
import 'package:pokedex/modules/poke_info/repository/poke_info_repository.dart';

part 'poke_info_event.dart';
part 'poke_info_state.dart';

class PokeInfoBloc extends Bloc<PokeInfoEvent, PokeInfoState> {
  PokeInfoBloc() : super(PokeInfoInitial()) {
    on<PokeInfoFetchEvent>(_fetch);
  }

  void _fetch(PokeInfoFetchEvent event, Emitter<PokeInfoState> emit) async {
    emit(PokeInfoFetching());

    PokeInfoModel? data = await PokeInfoRepository.fetchPokemon(event.id);

    if (data != null) {
      emit(PokeInfoFetchSuccess(data));
    } else {
      emit(PokeInfoFetchError());
    }
  }
}
