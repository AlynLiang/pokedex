part of 'poke_info_bloc.dart';

@immutable
abstract class PokeInfoState {}

class PokeInfoInitial extends PokeInfoState {}

class PokeInfoFetching extends PokeInfoState {}

class PokeInfoFetchSuccess extends PokeInfoState {
  final PokeInfoModel data;

  PokeInfoFetchSuccess(this.data);
}

class PokeInfoFetchError extends PokeInfoState {}
