class PokeInfoModel {
  final int id;
  final String name;
  final String spriteUrl;

  PokeInfoModel(
    this.id,
    this.name,
    this.spriteUrl,
  );

  PokeInfoModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        spriteUrl = json['sprites']['front_default'];
}
