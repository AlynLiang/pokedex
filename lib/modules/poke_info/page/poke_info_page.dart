import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:pokedex/modules/home/page/home_page.dart';

import 'package:pokedex/modules/poke_info/bloc/poke_info_bloc.dart';

class PokeInfoPage extends StatefulWidget {
  final String id;

  const PokeInfoPage({
    required this.id,
    super.key,
  });

  @override
  State<PokeInfoPage> createState() => _PokeInfoPageState();
}

class _PokeInfoPageState extends State<PokeInfoPage> {
  @override
  void initState() {
    BlocProvider.of<PokeInfoBloc>(context)
        .add(PokeInfoFetchEvent(int.tryParse(widget.id) ?? 0));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<PokeInfoBloc>(context)
        .add(PokeInfoFetchEvent(int.tryParse(widget.id) ?? 0));

    return LayoutBuilder(
      builder: (context, constraints) {
        return Scaffold(
          appBar: AppBar(
            leading: (constraints.maxWidth > 600)
                ? null
                : IconButton(
                    onPressed: () => GoRouter.of(context).go('/'),
                    icon: const Icon(Icons.arrow_back),
                  ),
          ),
          body: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (constraints.maxWidth > 600) ...[
                  Expanded(child: HomePage()),
                ],
                Expanded(
                  flex: 2,
                  child: BlocBuilder<PokeInfoBloc, PokeInfoState>(
                    builder: (context, state) {
                      if (state is PokeInfoFetching) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (state is PokeInfoFetchSuccess) {
                        return Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(state.data.id.toString()),
                              Text(state.data.name),
                              Image.network(
                                state.data.spriteUrl,
                                fit: BoxFit.cover,
                                width: 150,
                                height: 150,
                              ),
                            ],
                          ),
                        );
                      } else {
                        return const Center(child: Text('error'));
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
