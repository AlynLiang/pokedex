import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:pokedex/modules/poke_info/model/poke_info_model.dart';

mixin PokeInfoRepository {
  static Future<PokeInfoModel?> fetchPokemon(int id) async {
    PokeInfoModel? data;

    var url = Uri.parse('https://pokeapi.co/api/v2/pokemon/$id');
    final result = await http.get(url);

    if (result.statusCode == 200) {
      data = PokeInfoModel.fromJson(jsonDecode(result.body));
    }

    return data;
  }
}
